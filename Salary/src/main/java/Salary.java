
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;

import org.apache.spark.TaskContext;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.*;
import org.apache.spark.streaming.api.java.*;
import org.apache.spark.streaming.kafka010.*;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.text.ParseException;
import java.lang.*;

import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.Tuple5;
import scala.Tuple6;


/**

	Category
	   - 17
	18 - 24
	25 - 39
	40 - 

	@author 
	@version 1.0
*/
public class Salary
{

	public static int year;

	/**
	Main method
	*/
	public static void main( String[] args )
	{
				
		Calendar cal = Calendar.getInstance();
		year = 1900 + cal.getTime().getYear();


		SparkConf conf = new SparkConf()
			.setAppName("Salary");
 
		JavaSparkContext sc = new JavaSparkContext( conf );

	
		Map<String, Object> kafkaParams = new HashMap<>();
		kafkaParams.put("bootstrap.servers", "localhost:9092");
		kafkaParams.put("key.deserializer", StringDeserializer.class);
		kafkaParams.put("value.deserializer", StringDeserializer.class);
		kafkaParams.put("group.id", "use_a_separate_group_id_for_each_stream");
		kafkaParams.put("auto.offset.reset", "latest");
		kafkaParams.put("enable.auto.commit", false);

		/*JavaInputDStream<ConsumerRecord<String, String>> stream =
			KafkaUtils.createDirectStream(
			sc,
			LocationStrategies.PreferConsistent(),
			ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams)
		);*/

		/*for( int i=0; i<2; i++ )
		{
			System.out.println( i );			

			OffsetRange[] offsetRanges = {
			  // topic, partition, inclusive starting offset, exclusive ending offset
			  OffsetRange.create( "test", 0, i*5, (i+1)*5 ),
			};

			System.out.println("===================");

			JavaRDD<ConsumerRecord<String, String>> kafka_rdd = KafkaUtils.createRDD(
			  sc,
			  kafkaParams,
			  offsetRanges,
			  LocationStrategies.PreferConsistent()
			);

			System.out.println("===================2");

			JavaPairRDD<String, String> aaa = kafka_rdd.mapToPair( a -> new Tuple2<>(a.key(), a.value()));

			System.out.println("===================3");

			aaa.saveAsTextFile("/home/grey/hw2_kar/kafkaf"+new Integer(i).toString());

		}*/


	
		JavaRDD<String> src = sc.textFile("/home/grey/hw2_kar/input.txt");

		JavaRDD<Tuple3<Integer,Integer,Integer>> src_tab_fil = Stage0( src );

		JavaPairRDD<Integer,Double> sal = Stage1( src_tab_fil, 0 );
		JavaPairRDD<Integer,Double> abr = Stage1( src_tab_fil, 1 );

		sal.saveAsTextFile("/home/grey/hw2_kar/output_salary");	
		abr.saveAsTextFile("/home/grey/hw2_kar/output_abroad");		

		sc.stop();
	}


	public static JavaRDD<Tuple3<Integer,Integer,Integer>> Stage0( JavaRDD<String> src )
	{
		JavaRDD<Tuple3<Integer,Integer,Integer>> src_tab = src.map( a -> StringToCols( a ) );

		return src_tab.filter( a -> a._1() >= 0 );
	}


	public static JavaPairRDD<Integer,Double> Stage1(JavaRDD<Tuple3<Integer,Integer,Integer>> src_tab_fil, int type)
	{
		JavaRDD<Tuple3<Integer,Integer,Integer>> sal_0 = src_tab_fil.filter( a -> a._2() == type );

		JavaPairRDD<Integer,Tuple2<Integer,Integer>> sal_1 = sal_0.mapToPair( a -> new Tuple2<>( a._1(), new Tuple2<>( a._3() ,1 ) ) );
		
		JavaPairRDD<Integer,Tuple2<Integer,Integer>> sal_2 = sal_1.reduceByKey( (a,b) -> new Tuple2<>( a._1()+b._1(), a._2()+b._2() ) );

		return sal_2.mapToPair(
			a -> new Tuple2<>( a._1(), a._2()._1().doubleValue() / a._2()._2().doubleValue() ) );
	}


	public static Tuple3<Integer,Integer,Integer> StringToCols( String a )
	{

		int age = -1;
		int type = 0;
		int value = 0;

		String params[] = a.split(" ");

		if( params.length != 4 )
		{
			return new Tuple3<>( new Integer(age), new Integer(type), new Integer(value) );
		}

		try
		{
			age = Integer.parseInt( params[1] );
		}catch(NumberFormatException e)
		{ age=-1; return new Tuple3<>( new Integer(age), new Integer(type), new Integer(value) );}

		try
		{
			type = Integer.parseInt( params[2] );
		}catch(NumberFormatException e)
		{ age=-1; return new Tuple3<>( new Integer(age), new Integer(type), new Integer(value) );}

		try
		{
			value = Integer.parseInt( params[3] );
		}catch(NumberFormatException e)
		{ age=-1; return new Tuple3<>( new Integer(age), new Integer(type), new Integer(value) );}

		Calendar cal = Calendar.getInstance();
		age = year - age;
		
		if( age < 0 )
			age = -1;
		else if( age < 18 )
			age = 0;
		else if( age < 25 )
			age = 1;
		else if( age < 40 )
			age = 2;
		else
			age = 3;
		
		return new Tuple3<>( new Integer(age), new Integer(type), new Integer(value) );
	}

	/**
	@param - raw_log input RDD of string
	@return - reduced results
	*/
	/*public static JavaPairRDD<MsgKey, Integer> CalcRDD( JavaRDD<String> raw_log )
	{
		JavaPairRDD<MsgKey, Integer> messages = raw_log.mapToPair( a -> new Tuple2<>( new MsgKey(a), 1 ) );

		JavaPairRDD<MsgKey, Integer> messages_fil = messages.filter( a -> a._1().getTime() > 0 );

		JavaPairRDD<MsgKey, Integer> reduced = messages_fil.reduceByKey( (a,b) -> a+b );

		return reduced;
	}*/

}














