
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;

import org.junit.*;
import static org.junit.Assert.fail;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.text.ParseException;
import java.lang.Math.*;


import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.Tuple5;
import scala.Tuple6;


public class SalaryTest
{

    @Test
    public void testStringToCols()
	{
		Calendar cal = Calendar.getInstance();
		Salary.year = 1900 + cal.getTime().getYear();

		Tuple3<Integer,Integer,Integer> dst;


		dst = Salary.StringToCols( "123456 1997 0 70000" );
		
		if( dst._1().intValue() != 1 ) fail("age1");
		if( dst._2().intValue() != 0 ) fail("type1");
		if( dst._3().intValue() != 70000 ) fail("value1");


		dst = Salary.StringToCols( "1dsfsdfsdf" );
		
		if( dst._1().intValue() != -1 ) fail("age2");


		dst = Salary.StringToCols( "123456 2020 0 70000" );
		
		if( dst._1().intValue() != -1 ) fail("age3");


		dst = Salary.StringToCols( "123456 1980 1 2" );
		
		if( dst._1().intValue() != 2 ) fail("age4");
		if( dst._2().intValue() != 1 ) fail("type4");
		if( dst._3().intValue() != 2 ) fail("value4");

		//if( mk.getTime() != 0 )
		//fail("MsgKey: incorrect");
	}


    @Test
    public void testSparkIncorrect()
	{
		Calendar cal = Calendar.getInstance();
		Salary.year = 1900 + cal.getTime().getYear();

		SparkConf conf = new SparkConf()
			.set("spark.driver.host", "localhost")
			.set("spark.testing.memory", "2147480000");

		JavaSparkContext sc = new JavaSparkContext( "local[1]", "LogStatTest", conf );

		List<String> dataset = Arrays.asList
		(
			new String("123456 1997 0 10000")
		);

		HashMap<Integer, Double> results = new HashMap<Integer, Double>();
		results.put( new Integer( 1 ), new Double( 10000 ) );


		/*JavaRDD<String> raw_log = sc.parallelize( dataset );

		JavaPairRDD<MsgKey, Integer> reduced = LogStat.CalcRDD( raw_log );

		List<Tuple2<MsgKey, Integer>> out = reduced.collect();*/



		JavaRDD<String> src = sc.parallelize( dataset );

		JavaRDD<Tuple3<Integer,Integer,Integer>> src_tab_fil = Salary.Stage0( src );

		JavaPairRDD<Integer,Double> sal = Salary.Stage1( src_tab_fil, 0 );
		//JavaPairRDD<Integer,Double> abr = Salary.Stage1( src_tab_fil, 1 );

		List<Tuple2<Integer,Double>> out = sal.collect();



		if( out.size() != 1 )
			fail("Incorrect results size");

		for(Iterator<Tuple2<Integer,Double>> i = out.iterator(); i.hasNext();)
		{
			Tuple2<Integer,Double> item = i.next();

			if( ! results.containsKey( item._1() ) )
				fail("Cant find MsgKey");

			//System.out.println( item._1().toString() );
			//System.out.println( item._2().doubleValue() );
			//System.out.println( results.get( item._1() ).doubleValue() );

			if( Math.abs( results.get( item._1() ).doubleValue() - item._2().doubleValue() ) > 0.0001 )
				fail("Incorrect count");

		}

		sc.stop();
	}

}














